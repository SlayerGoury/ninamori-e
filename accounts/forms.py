from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

error_messages = {
	'username': {
		'required': _('Username is required'),
	},
	'password': {
		'required': _('Password is required'),
	},
	'password_confirm': {
		'required': _('Confirmation is required'),
	},
	'email': {
		'required': _('Email address is required'),
		'invalid': _('Email address is invalid'),
	},
	'first_last_name': {
		'invalid': _('This is too long (limit is %(limit_value)s)'),
	},
	'code': {
		'requered': _('Code is requered'),
	},
}


class registration_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          widget=forms.TextInput( attrs={'class': 'input', 'onblur': 'username_checker();', 'required': True} ), required=True)
	password                            = forms.CharField(error_messages=error_messages['password'],          required=True, widget=forms.PasswordInput( attrs={'class': 'input', 'required': True} ))
	password_confirm                    = forms.CharField(error_messages=error_messages['password_confirm'],  required=True, widget=forms.PasswordInput( attrs={'class': 'input', 'required': True} ))
	email                               = forms.EmailField(error_messages=error_messages['email'],            required=True, widget=forms.TextInput( attrs={'class': 'input', 'required': True} ))

class login_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          required=True)
	password                            = forms.CharField(error_messages=error_messages['password'],          required=True, widget=forms.PasswordInput)
	redirect                            = forms.CharField(                                                    required=False)

class manage_form (forms.Form):
	first_name                          = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={'class': 'input'} ),     required=False, max_length=30)
	last_name                           = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={'class': 'input'} ),     required=False, max_length=30)
	password                            = forms.CharField(                                                    widget=forms.PasswordInput( attrs={'class': 'input'} ), required=False, initial=None)
	new_password                        = forms.CharField(                                                    widget=forms.PasswordInput( attrs={'class': 'input'} ), required=False, initial=None)
	new_password_confirm                = forms.CharField(                                                    widget=forms.PasswordInput( attrs={'class': 'input'} ), required=False, initial=None)

class restore_form (forms.Form):
	username                            = forms.CharField(                                                    widget=forms.TextInput( attrs={'class': 'input'} ), required=False)
	email                               = forms.EmailField(error_messages=error_messages['email'],            widget=forms.TextInput( attrs={'class': 'input'} ), required=False)
	restore                             = forms.CharField(                                                    required=False)

class restore_code_form (forms.Form):
	code                                = forms.CharField(error_messages=error_messages['code'],              widget=forms.TextInput( attrs={'class': 'input', 'required': True} ), required=True)
	restore                             = forms.CharField(                                                    required=False)

class restore_set_password_form(forms.Form):
	password                            = forms.CharField(error_messages=error_messages['password'],          required=True, widget=forms.PasswordInput( attrs={'class': 'input', 'required': True} ))
	password_confirm                    = forms.CharField(error_messages=error_messages['password_confirm'],  required=True, widget=forms.PasswordInput( attrs={'class': 'input', 'required': True} ))
	restore                             = forms.CharField(                                                    required=False)
