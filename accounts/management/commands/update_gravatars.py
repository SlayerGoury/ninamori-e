from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from accounts import utils_gravatar


class Command(BaseCommand):
	help = 'Download updates for all gravatars'

	def add_arguments(self, parser):
		parser.add_argument('--rating',
			dest='rating',
			default=settings.GRAVATAR_RATING,
			help='maximum rating allowed')
		parser.add_argument('--default',
			dest='default',
			default=settings.GRAVATAR_DEFAULT,
			help='default gravatar type')
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Only count, do not download anything')
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Output only summary')
		parser.add_argument('--nosummary',
			action='store_true',
			dest='nosummary',
			default=False,
			help='do not output summary')
		parser.add_argument('--id',
			dest='id',
			default='',
			help='process just one user')

	def handle(self, *args, **options):
		update = not options['fake']
		rating = options['rating']
		default = options['default']
		silent = options['silent']
		nosummary = options['nosummary']
		user_id = options['id']
		counter_users = 0
		counter_updates = 0

		user_list = [User.objects.get(id=user_id)] if user_id else User.objects.all()

		emails = []
		for user in user_list:
			counter_users += 1
			if user.userprofile.confirmation_email_valid:
				emails.append(user.email)

		gravatars = utils_gravatar.get_gravatars_list_for_mass_download(emails)
		utils_gravatar.threaded_gravatars_download(gravatars, silent)

		if not nosummary: self.stdout.write('Processed '+ str(counter_users) +' users')
