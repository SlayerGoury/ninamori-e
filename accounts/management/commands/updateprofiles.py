from django.core.management.base import BaseCommand, CommandError
from ninamori.utils import update_all_users


class Command(BaseCommand):
	help = 'Create extra profiles for every lacking account'

	def add_arguments(self, parser):
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Only count, do not perform actual update')

	def handle(self, *args, **options):
		update = not options['fake']
		result = update_all_users(update)
		self.stdout.write('Checked ' + str(result[0]) + ' users, updated ' + str(result[1]) + ' of them')
		self.stdout.write('Update = ' + str(update))
