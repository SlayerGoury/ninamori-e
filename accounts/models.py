from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
import datetime


class UserProfile (models.Model):
	user                                = models.OneToOneField(User, on_delete=models.CASCADE)
	confirmation_email_sent             = models.DateTimeField(blank=True, null=True)
	confirmation_email_code             = models.CharField(max_length=200, default=None, blank=True, null=True)
	confirmation_email_valid            = models.BooleanField(default=False)

	authorization_sha                   = models.CharField(max_length=41, default=None, blank=True, null=True)
	authorization_key                   = models.CharField(max_length=255, default=None, blank=True, null=True)
	authorization_key_removed           = models.DateTimeField(blank=True, null=True)

	restore_code                        = models.CharField(max_length=255, default=None, blank=True, null=True)
	restore_request_made                = models.DateTimeField(blank=True, null=True)

	class Meta:
		permissions = (
			('access_accounts_management', 'Can manage own account'),
			('manage_authorization_key', 'Can manage own authorization key'),
		)

	def authorization_key_ready (self):
		if userprofile.authorization_key_removed:
			if (timezone.now() - self.authorization_key_removed).total_seconds() > settings.AUTHORIZATION_KEY_REMAKE_DELAY:
				return True
		return False

	def confirmation_code_ready (self):
		if not self.confirmation_email_code:
			return True
		if self.confirmation_email_valid:
			return False
		if (timezone.now() - self.confirmation_email_sent).total_seconds() > settings.EMAIL_CONFIRMATION_RESEND_DELAY:
			return True
		return False

	def __str__ (self):
		return self.user.username


@receiver(post_save, sender=User)
def create_user_profile (sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.create(user=instance)
