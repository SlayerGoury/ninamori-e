from django.conf import settings
from django.conf.urls import include, url
from . import views


urlpatterns = [
	url(r'^$',                         views.index,                         name='accounts index'),
#	url(r'admin$',                     views.admin_index,                   name='accounts admin index'),
#	url(r'admin_create_user$',         views.web_create_user_by_admin,      name='accounts admin create'),
	url(r'manage$',                    views.manage,                        name='accounts manage'),
	url(r'key_authenticate$',          views.web_key_authenticate,          name='accounts key_authenticate'),
#	url(r'key/(?P<key>.+)$',           views.web_key_authenticate,          name='accounts key_url_authenticate'),
	url(r'login$',                     views.login,                         name='accounts login'),
	url(r'logout$',                    views.logout,                        name='accounts logout'),
	url(r'confirm_email$',             views.confirm_email,                 name='accounts confirm_email'),
	url(r'check_username$',            views.check_username,                name='accounts check_username'),
	url(r'register$',                  views.web_register,                  name='accounts register'),
#	url(r'make_authorization_key$',    views.web_make_authorization_key,    name='accounts make_authorization_key'),
#	url(r'remove_authorization_key$',  views.web_remove_authorization_key,  name='accounts remove_authorization_key'),
	url(r'restore$',                   views.web_restore,                   name='accounts restore'),
]
