from django.conf import settings
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import User, Permission
from django.urls import reverse
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext as _
from django.utils.translation import get_language
import django.utils.timezone

from time import strftime, localtime, mktime, strptime
from accounts import utils_gravatar as ug
from ninamori.utils import async_email, validate_email, download_file
from .models import *

import datetime
import hashlib
import logging

logger = logging.getLogger(__name__)


def api_register (username, email, password, perms=[]):
	if User.objects.filter(username=username).exists():
		return {
			'status': 'error',
			'result': 'username exists'
		}
	if not validate_email(email):
		return {
			'status': 'error',
			'result': 'invalid email'
		}
	if User.objects.filter(email=email).exists():
		return {
			'status': 'error',
			'result': 'email exists'
		}
	user = User.objects.create_user(username, email, password)
	for perm in perms:
		user.user_permissions.add(Permission.objects.get(content_type__app_label=perm[0], codename=perm[1]))
	# Gravatar update gone to email confirmation
	make_and_send_confirmation_code(user)  # this should also make sure user.userprofile is created before first login
	return {
		'status': 'ok',
		'result': 'user ' + username + ' created'
	}


def get_data_for_email (user):
	return {
		'protocol': settings.PROTOCOL_SCHEME,
		'site_domain': settings.SITE_DOMAIN,
		'app_index': reverse('accounts index'),
		'app_confirm': reverse('accounts confirm_email'),
		'username': user.username,
	}


def make_and_send_confirmation_code (user):
	code = get_random_string(length=32)
	user.userprofile.confirmation_email_sent = django.utils.timezone.now()
	user.userprofile.confirmation_email_code = make_password(code)
	user.userprofile.confirmation_email_valid = False
	user.userprofile.save()

	subject = _('[%(site_name)s] Email address confirmation' % {'site_name': settings.SITE_NAME[get_language()]})
	template = 'accounts/email_confirmation.txt'
	data = get_data_for_email(user)
	data['code'] = code
	message = render_to_string(template, data)

	async_email(
		subject = subject,
		message = message,
		recipient_list = [user.email],
		html_message = message
	)


def confirm_email (user, code, force=False):
	if check_password(code, user.userprofile.confirmation_email_code) or force:
		user.userprofile.confirmation_email_valid = True
		user.userprofile.save()
		ug.threaded_gravatars_download( ug.get_gravatars_list_for_mass_download([user.email]), silent=True )
		return True
	return False


def make_and_send_authorization_key (user, email=None):
	authorization_sha = hashlib.sha1(str(user.id)).hexdigest()
	authorization_key = authorization_sha + get_random_string(length=128)
	user.userprofile.authorization_sha = authorization_sha
	user.userprofile.authorization_key = make_password(authorization_key)
	user.userprofile.save()

	title = _('[%(site_name)s] Authorization key' % {'site_name': settings.SITE_NAME[get_language()]})
	template = 'accounts/authorization_key.txt'
	email_message = MailerMessage()
	email_message.subject       = title
	email_message.content       = render_to_string(template, get_data_for_email(user, key=authorization_key))
	email_message.from_address  = settings.DEFAULT_FROM_EMAIL
	email_message.to_address    = email or user.email
	email_message.app           = 'accounts authorization code'
	email_message.save()

	return authorization_key


def make_and_send_restoration_key (user):
	code = get_random_string(length=32)
	user.userprofile.restore_code = make_password(code)
	user.userprofile.restore_request_made = django.utils.timezone.now()
	user.userprofile.save()

	subject = _('[%(site_name)s] Account restoration requested' % {'site_name': settings.SITE_NAME[get_language()]})
	template = 'accounts/restoration_code.txt'
	data = get_data_for_email(user)
	data['code'] = code
	message = render_to_string(template, data)

	async_email(
		subject = subject,
		message = message,
		recipient_list = [user.email],
		html_message = message
	)
	return code


def restore (username, email, delay):
	if not username or email:
		return {'status': 'no data'}

	try:
		if username:
			user = User.objects.get(username=username)
		elif email:
			user = User.objects.get(email=email)
	except User.DoesNotExist:
		return {'status': 'false data'}

	if username and email:
		try:
			assert user.username == username
			assert user.email == email
		except AssertionError:
			return {'status': 'mismatch'}

	if user.userprofile.restore_request_made:
		if (django.utils.timezone.now() - user.userprofile.restore_request_made).total_seconds() < delay:
			return {'status': 'too soon'}

	restore_code = make_and_send_restoration_key(user)
	return {'status': 'success', 'user_id': user.id, 'code': restore_code}


def veryfy_code (code, str_id, delay):
	try:
		userprofile = UserProfile.objects.get(user_id=str_id)
		assert check_password(code, userprofile.restore_code) == True
		assert (django.utils.timezone.now() - userprofile.restore_request_made).total_seconds() < delay
	except (UserProfile.DoesNotExist, ValueError, AssertionError):
		return False

	return True
