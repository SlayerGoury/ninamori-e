from django.conf import settings

from ninamori.utils import download_file

import hashlib
import locale
import logging
import threading
import urllib
from os import stat, path
from time import strftime, localtime, mktime, strptime
from urllib.request import Request, urlopen
from queue import Queue

logger = logging.getLogger(__name__)


# This class made with help of Anurag Uniyal form stackoverflow
class DownloadGravatarThread (threading.Thread):
	def __init__(self, queue, silent):
		super(DownloadGravatarThread, self).__init__()
		self.daemon = True
		self.queue = queue
		self.silent = silent

	def run(self):
		while True:
			gravatar = self.queue.get()
			try:
				self.download_gravatar_if_updated(gravatar)
			except Exception as e:
				logger.warning('Exception: %s' % str(e))
			self.queue.task_done()

	def download_gravatar_if_updated (self, gravatar):
		# setlocale here is fine because it should always be en_US.UTF-8
		locale.setlocale(locale.LC_ALL, str('en_US.UTF-8'))
		email = gravatar[0]
		file_path = gravatar[1]
		url = gravatar[2]
		updated = gravatar[3]
		request = urlopen(Request(url))
		modified = mktime(strptime(request.info().get_all("Last-Modified")[0], "%a, %d %b %Y %H:%M:%S GMT"))
		print_path = file_path.split('/')[-1].split('.')[0]
		if not updated or modified > updated:
			if not self.silent: print('Downloading [' + print_path + '], email: [' + email+ ']')
			download_file(url, file_path)
		else:
			if not self.silent: print('Passing     [' + print_path + '], email: [' + email+ ']')


def get_gravatar_hash (email):
	return hashlib.md5(email.lower().encode('utf-8')).hexdigest()


def get_gravatar_url (gravatar, size):
	email = gravatar[0]
	rating = gravatar[1]
	default = gravatar[2]
	gravatar_url = settings.GRAVATAR_URL_PREFIX + get_gravatar_hash(email) + '?'
	gravatar_url += urllib.parse.urlencode({'d': default, 's': str(size), 'r': rating})
	return gravatar_url


def get_gravatar_file_path (email_hash, size):
	return settings.MEDIA_ROOT + '/avatars/gravatar_cache/' + email_hash + '_' + str(size) + '.png'


# TODO move this directly into threaded_gravatars_download
def get_gravatars_list_for_mass_download (emails):
	"""
	result should look like this:
	[
		(email, file_path, url, updated),
		...
	]
	"""
	result = []
	for email in emails:
		for size in settings.GRAVATAR_SIZES:
			file_path = get_gravatar_file_path(get_gravatar_hash(email),size)
			url = get_gravatar_url((email, settings.GRAVATAR_RATING, settings.GRAVATAR_DEFAULT), size)
			updated = stat(file_path).st_mtime if path.exists(file_path) else None
			result.append( (email, file_path, url, updated) )

	return result


def threaded_gravatars_download (gravatars, silent):
	queue = Queue()
	for gravatar in gravatars:
		queue.put(gravatar)

	for i in range(len(gravatars)):
		t = DownloadGravatarThread(queue, silent)
		t.start()

	queue.join()
