from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.utils.crypto import get_random_string
from django.views.decorators.http import require_http_methods

from .forms import *
from .models import *
from . import utils

import datetime
import hashlib
import logging

logger = logging.getLogger(__name__)


def index (request):
	user = request.user
	form_errors = request.session.pop('form errors') if 'form errors' in request.session else None

	initial = {
		'first_name': user.first_name,
		'last_name': user.last_name,
	} if user.is_authenticated else {}

	render_data = {
		'redirect': request.GET.get('next') or request.META.get('HTTP_REFERER') or '/',
		'key_auth': request.GET.get('key_auth'),
		'register': request.GET.get('register'),
		'code': request.GET.get('code'),
		'registration_form': registration_form,
		'manage_form': manage_form(initial=initial),
		'form_errors': form_errors,
	}
	return render(request, 'accounts/index.html', render_data)


@require_http_methods(['POST'])
def login (request):
	form = login_form(request.POST or None)
	if not form.is_valid():
		request.session['form errors'] = {'login': form.errors}
		return redirect('accounts index')

	data = form.cleaned_data
	user = authenticate(username=data['username'], password=data['password'])
	if user is not None and user.is_active:
		auth.login(request, user)
		destination = data.get('redirect') or reverse('accounts index')
		return redirect(destination)
	else:
		request.session['form errors'] = {'auth': _('Username or password mismatch')}
		return redirect('accounts index')


@require_http_methods(['POST'])
def logout (request):
	if request.user.is_authenticated:
		auth.logout(request)
		response = redirect(request.META.get('HTTP_REFERER') or '/')
		if request.COOKIES.get('mod_buttons'):
			response.set_cookie('mod_buttons', '')
		return response
	else:
		return render(request, '404.html', {'message': _('Logging out is only for those who logged in')}, status=400)


def check_username (request):
	if not request.GET.get('username'):
		return render(request, '404.html', {'message': _('You shall not pass')}, status=405)
	if User.objects.filter(username=request.GET.get('username')).exists():
		return HttpResponse('/static/img/accounts/check_username_bad.png', content_type='text/plain')
	return HttpResponse('/static/img/accounts/check_username_ok.png', content_type='text/plain')


@login_required
def confirm_email (request):
	user = request.user
	if request.method == 'POST':
		if not user.userprofile.confirmation_code_ready():
			request.session['form errors'] = {'confirmation_code': _('Confirmation code can not be remade yet')}
			return redirect('accounts index')

		utils.make_and_send_confirmation_code(user)
		return redirect('accounts index')

	code = request.GET.get('code')
	if not code:
		return render(request, '404.html', {'message': _('Get out and come back with a proper request')}, status=400)
	if utils.confirm_email(user, code):
		logger.info('[{} {}] confirmed email address [{}]'.format(user.id, user.username, user.email))
		return redirect('accounts index')
	else:
		return render(request, '403.html', {'message': _('Bad code')}, status=403)


def _register (form, perms):
	if not form.is_valid():
		return False, form
	data = form.cleaned_data
	if not data['password'] == data['password_confirm']:
		form.add_error('password_confirm', _('Passwords mismatch'))
		return False, form
	register = utils.api_register(data['username'], data['email'], data['password'], perms)
	logger.info('registration attempt [{} {}]'.format(data['username'], data['email']))
	return register, form


@require_http_methods(['POST'])
def web_register (request):
	form = registration_form(request.POST or None)
	register, form = _register(form, perms=[('accounts', 'access_accounts_management')])
	if register:
		if register['status'] == 'ok':
			user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
			auth.login(request, user)
			return redirect('accounts index')

		if register['status'] == 'error':
			if register['result'] == 'username exists':
				form.add_error('username', _('Username already exists'))
			if register['result'] == 'email exists':
				form.add_error('email', _('Email is already registered'))

	request.session['form errors'] = {'register': form.errors}
	return redirect(reverse('accounts index')+'?register=1')


def web_restore (request):
	step = request.GET.get('step')
	step = 1 if step not in ('1', '2', '3', '4') else int(step)
	if step == 4:
		return redirect('accounts index')

	if request.user.is_authenticated:
		return render(request, '404.html', {'message': _('You should not attempt to restore what you have not lost yet')}, status=400)

	restore_requested = request.session.get('accounts_restore_requested')
	if step == 1 and restore_requested:
		if (datetime.datetime.now() - datetime.datetime.strptime(restore_requested, settings.TIME_FORMAT)).total_seconds() > settings.ACCOUNT_RESTORATION_DELAY:
			request.session['accounts_restore_requested'] = None
		else:
			step = 2

	forms = {
		1: restore_form,
		2: restore_code_form,
		3: restore_set_password_form,
	}
	if request.method == 'GET':
		return render(request, 'accounts/restore.html', {
		'form': forms[step],
		'step': step,
	})

	if request.method == 'POST':
		if step == 1:
			form = restore_form(request.POST or None)
			status, form = _restore_step1(request, form)
		if step == 2:
			form = restore_code_form(request.POST or None)
			status, form = _restore_step2(request, form)
		if step == 3:
			form = restore_set_password_form(request.POST or None)
			status, form = _restore_step3(request, form)

		if not status:
			return render(request, 'accounts/restore.html', {'form': form, 'step': step})
		else:
			return redirect(reverse('accounts restore')+'?step='+str(step+1))


def _restore_step1 (request, form):
	""" Request a code byt entering either a username or an email address """
	if not form.is_valid():
		return False, form
	data = form.cleaned_data
	restore = utils.restore(data['username'], data['email'], settings.ACCOUNT_RESTORATION_DELAY)
	logger.info('restoration attempt [{} {}]'.format(data['username'], data['email']))
	if not restore['status'] == 'success':
		form.add_error('restore', _('Not enough or too much data'))
		return False, form
	request.session['accounts_restore_requested'] = datetime.datetime.now().strftime(settings.TIME_FORMAT)
	user_id = restore['user_id']
	if user_id:
		request.session['accounts_restore_user_id'] = str(user_id)
	return True, form


def _restore_step2 (request, form):
	""" Enter the code, confirming that you have access to the email account """
	if not form.is_valid():
		return False, form
	if not request.session.get('accounts_restore_requested'):
		form.add_error('restore', _('Restoration session has expired or was not started'))
		return False, form
	if not (datetime.datetime.now() - datetime.timedelta(seconds=settings.ACCOUNT_RESTORATION_DELAY)) < datetime.datetime.strptime(request.session.get('accounts_restore_requested'), settings.TIME_FORMAT):
		form.add_error('restore', _('Restoration session has expired'))
		return False, form
	data = form.cleaned_data
	if utils.veryfy_code(data['code'], request.session.get('accounts_restore_user_id'), settings.ACCOUNT_RESTORATION_DELAY):
		request.session['accounts_restore_approved'] = datetime.datetime.now().strftime(settings.TIME_FORMAT)
		return True, form
	else:
		form.add_error('restore', _('Wrong answer'))
		return False, form


def _restore_step3 (request, form):
	""" Reset password """
	if not form.is_valid():
		return False, form
	if not request.session.get('accounts_restore_approved'):
		form.add_error('restore', _('Restoration session has expired or was not started'))
		return False, form
	if not (datetime.datetime.now() - datetime.timedelta(seconds=settings.ACCOUNT_RESTORATION_DELAY)) < datetime.datetime.strptime(request.session.get('accounts_restore_approved'), settings.TIME_FORMAT):
		form.add_error('restore', _('Restoration session has expired'))
		return False, form
	data = form.cleaned_data
	if not data['password'] == data['password_confirm']:
		form.add_error('new_password_confirm', _('Passwords mismatch'))
		return False, form
	request.session['accounts_restore_approved'] = None
	request.session['accounts_restore_requested'] = None
	user = User.objects.get(id=request.session.get('accounts_restore_user_id'))
	user.set_password(data['password'])
	user.save()
	user = authenticate(username=user.username, password=data['password'])
	if user is not None and user.is_active:
		auth.login(request, user)
		if not user.userprofile.confirmation_email_valid:
			utils.confirm_email(user, None, True)
	return True, form


@login_required
@permission_required('accounts.access_accounts_management')
@require_http_methods(['POST'])
def manage (request):
	user = request.user
	form = manage_form(request.POST or None)
	valid = form.is_valid()
	request.session['form errors'] = {'manage': form.errors}
	if valid:
		data = form.cleaned_data
		password = data['password']
		passwords_match = data['new_password'] == data['new_password_confirm']

		if password:
			if user.check_password(password) and passwords_match:
				user.set_password(data['new_password'])
				data['password_updated'] = True
			elif not user.check_password(password):
				form.add_error('password', _('Invalid password'))
			elif not passwords_match:
				form.add_error('new_password_confirm', _('Passwords mismatch'))

		user.first_name = data['first_name']
		user.last_name = data['last_name']
		user.save()
		request.session['form errors']['manage']['updated'] = True

		if data.get('password_updated'):
			user = authenticate(username=user.username, password=data['new_password'])
			auth.login(request, user)

	return redirect('accounts index')


def web_key_authenticate (request, key=None):
	pass
