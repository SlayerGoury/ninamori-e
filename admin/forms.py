from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

error_messages = {
	'username': {
		'required': _('Username is required'),
	},
	'password': {
		'required': _('Password is required'),
	},
	'password_confirm': {
		'required': _('Confirmation is required'),
	},
	'email': {
		'required': _('Email address is required'),
		'invalid': _('Email address is invalid'),
	},
	'first_last_name': {
		'invalid': _('This is too long (limit is %(limit_value)s)'),
	},
	'code': {
		'requered': _('Code is requered'),
	},
}


class create_user_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          required=True, widget=forms.TextInput( attrs={'required': True} ))
	email                               = forms.EmailField(error_messages=error_messages['email'],            widget=forms.TextInput( attrs={} ), required=True)
	first_name                          = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={} ), required=False, max_length=30)
	last_name                           = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={} ), required=False, max_length=30)
	group                               = forms.CharField(                                                    required=False)

class edit_user_form (forms.Form):
	username                            = forms.CharField(error_messages=error_messages['username'],          required=False, widget=forms.TextInput( attrs={'required': False} ))
	email                               = forms.EmailField(error_messages=error_messages['email'],            widget=forms.TextInput( attrs={} ), required=False)
	first_name                          = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={} ), required=False, max_length=30)
	last_name                           = forms.CharField(error_messages=error_messages['first_last_name'],   widget=forms.TextInput( attrs={} ), required=False, max_length=30)
	active                              = forms.BooleanField(required=False)
	staff                               = forms.BooleanField(required=False)

class user_permission_or_group (forms.Form):
	permission_codename                 = forms.CharField(                                                    required=False, widget=forms.TextInput( attrs={'required': False} ))
	group_name                          = forms.CharField(                                                    required=False, widget=forms.TextInput( attrs={'required': False} ))
	add_if_true_else_remove             = forms.BooleanField(required=False)
