from django.db import models
from django.contrib.auth.models import User as django_user


class User(django_user):
	class Meta:
		proxy = True
		permissions = (
			('list_users',        'Can view and search and sort throug the list of users'),
			('edit_users',        'Can edit users'),
			('create_users',      'Can creane new accounts'),
			('edit_staff',        'Can edit staff members'),
			('grant_staff',       'Can grant staff access'),
			('protected_staff',   'Can only be edited by superuser'),
			('list_permissions',  'Can view the list of permissions'),
			('list_groups',       'Can view the list of groups'),
		)

	def can_be_managed_by(self, user):
		if not user.has_perm('auth.edit_users'):
			return False
		if self.is_staff and not user.has_perm('auth.edit_staff'):
			return False
		if self.has_perm('auth.protected_staff') and not user.is_superuser:
			return False
		return True
