from django import template

register = template.Library()


@register.simple_tag
def can_a_edit_b (a, b):
	return b.can_be_managed_by(a)
