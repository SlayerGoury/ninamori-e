from django.conf import settings
from django.conf.urls import include, url
from django.urls import path
from . import views
from ninamori.decorators import staff_only


urlpatterns = [
	path('',                         staff_only(views.index),             name='admin index'),
	path('user_list',                staff_only(views.user_list),         name='admin user_list'),
	path('user/<str:user_id>/',      staff_only(views.manage_user),       name='admin edit_user'),
]
