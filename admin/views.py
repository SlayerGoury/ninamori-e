from django.contrib.auth.decorators import permission_required
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.translation import ugettext as _

from ninamori.utils import get_object_or_none
from accounts.utils import make_and_send_confirmation_code
from .models import User
from .forms import *

import logging

logger = logging.getLogger(__name__)


def index (request):
	user_list = User.objects.all().order_by('-date_joined')[:10] if request.user.has_perm('auth.list_users') else None
	return render(request, 'admin/index.html', {
		'user_list': user_list,
		'active': None,
		'staff': None,
		'email_confirmed': None,
	})


def _yes_no_default (value):
	if value in ('yes', 'no'):
		return value
	else:
		return False


@permission_required('auth.list_users')
def user_list (request):
	try:
		page = int(request.GET.get('page'))
	except TypeError:
		page = 1
	order_by = request.GET.get('order_by')
	if order_by not in (
		'date_joined',
		'-date_joined',
		'username',
		'-username',
		'last_name',
		'-last_name',
		'first_name',
		'-first_name',
		'email',
		'-email',
	):
		order_by = '-date_joined'
	active = _yes_no_default(request.GET.get('active'))
	staff = _yes_no_default(request.GET.get('staff'))
	email_confirmed = _yes_no_default(request.GET.get('email_confirmed'))

	query = Q()
	if active:
		if active == 'yes':
			query &= Q(is_active=True)
		else:
			query &= Q(is_active=False)
	if staff:
		if staff == 'yes':
			query &= Q(is_staff=True)
		else:
			query &= Q(is_staff=False)
	if email_confirmed:
		if email_confirmed == 'yes':
			query &= Q(userprofile__confirmation_email_valid=True)
		else:
			query &= Q(userprofile__confirmation_email_valid=False)

#	user_list = User.objects.all().order_by(order_by)[(page-1)*20:(page-1)*20+20]
	pages = (User.objects.filter(query).count()+19)//20  # integer division rounding up
	user_list = User.objects.filter(query).order_by(order_by)[(page-1)*20:(page-1)*20+20]

	return render(request, 'admin/user_list.html', {
		'user_list': user_list,
		'order_by': order_by,
		'page': page,
		'pages': range(1, pages+1),
		'active': active or 'void',
		'staff': staff or 'void',
		'email_confirmed': email_confirmed or 'void',
	})


def _log_edit_attempt (user, managed_user, data):
	logger.info(
		'[{}] attempted to edit account [{}], old data:\n'.format(user.id, managed_user.id)
		+'username [{}]\n'.format(managed_user.username)
		+'first name [{}]\n'.format(managed_user.first_name)
		+'last name [{}]\n'.format(managed_user.last_name)
		+'email [{}]\n'.format(managed_user.email)
		+'active [{}]\n'.format(managed_user.is_active)
		+'staff [{}]\n'.format(managed_user.is_staff)
		+'new data:\n'
		+'username [{}]\n'.format(data['username'])
		+'first name [{}]\n'.format(data['first_name'])
		+'last name [{}]\n'.format(data['last_name'])
		+'email [{}]\n'.format(data['email'])
		+'active [{}]\n'.format(data['active'])
		+'staff [{}]\n'.format(data['staff'])
	)


def _manage_user (form, user, managed_user):
	if not form.is_valid():
		return False, form
	data = form.cleaned_data
	_log_edit_attempt(user, managed_user, data)

	if data['username'] != managed_user.username:
		if User.objects.filter(username=data['username']).exists():
			form.add_error('username', _('Username is taken'))
			return False, form
		managed_user.username = data['username']

	if data['email'] != managed_user.email:
		managed_user.email = data['email']
		make_and_send_confirmation_code(managed_user)

	managed_user.first_name = data['first_name']
	managed_user.last_name = data['last_name']
	managed_user.is_active = data['active']
	managed_user.is_staff = data['staff']
	managed_user.save()
	return True, form


@permission_required('auth.edit_users')
def manage_user (request, user_id):
	managed_user = get_object_or_none(User, id=user_id)
	if not managed_user:
		return render(request, '404.html', {'message': _('User doesn\'t seem to exist in the database')}, status=404)
	if not managed_user.can_be_managed_by(request.user):
		return render(request, '404.html', {'message': _('This user can\'t be managed by you')}, status=403)

	if request.method == 'GET':
		initial = {
			'user_id': user_id,
			'username': managed_user.username,
			'email': managed_user.email,
			'first_name': managed_user.first_name,
			'last_name': managed_user.last_name,
			'active': managed_user.is_active,
			'staff': managed_user.is_staff,
		}
		form = edit_user_form(initial=initial)
		return render(request, 'admin/user.html', {'managed_user': managed_user, 'form': form})

	if request.method == 'POST':
		form = edit_user_form(request.POST or None)
		valid, form = _manage_user(form, request.user, managed_user)
		if not valid:
			return render(request, 'admin/user.html', {'managed_user': managed_user, 'form': form}, status=400)
		else:
			return redirect(reverse( 'admin edit_user', kwargs={'user_id': user_id} ))
