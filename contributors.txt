This is a list of known contributors
If you contribute something to the project — add something about you here
If you do not want to share your contact information — write a letter to Goury or be anonymous
All anonymous contributors delegate their authority to Goury


Goury Gabriev (goury@slayers.ru)
	Maintainer
	Project assembly
	Python
	Html
	Typo fixing
