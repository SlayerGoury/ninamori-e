from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from . import views


def staff_only (func):
	@login_required
	def wrapper(*args, **kwargs):
		if not args[0].user.is_staff:
			return views.denied(args[0], message=_('Staff only'))
		return func(*args, **kwargs)
	return wrapper
