from django.conf import settings
from django.conf.urls import include, url
from django.views import static

from . import views

urlpatterns = [
	url(r'^id/',             include('accounts.urls')),
	url(r'^sanctum/',        include('admin.urls')),
	url(r'404$',             views.not_found, name='404'),
	url(r'403$',             views.denied, name='403'),
	url(r'robots.txt$',      views.robots, name='robots'),
	url(r'sitemap.xml$',     views.sitemap, name='sitemap'),
	url(r'adminhelp$',       views.adminhelp, name='adminhelp'),
#	url(r'^$',               cms_views.index, name='index'),
	url(r'^$',               views.index, name='index'),
	url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT,}),
]
