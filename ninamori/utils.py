from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_email as django_validate_email
from django.shortcuts import render
from django.template import Context, Template
from django.utils import timezone
from django.utils.decorators import available_attrs
from django.utils.translation import ugettext as _
from accounts.models import UserProfile
import logging
import re
import requests
from os import devnull, makedirs
from subprocess import call
from threading import Thread

NULL = open(devnull, 'w')
cache_settings = settings.CACHES['default']
logger = logging.getLogger(__name__)
uuid4hex = re.compile('[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}\Z', re.I)


def validate_email (email):
	try:
		django_validate_email(email)
		return True
	except ValidationError:
		return False


def get_name (user):
	if not user or not user.is_authenticated():
		return _('Anonymous')
	if user.first_name and user.last_name:
		return user.first_name+' '+user.last_name
	if user.first_name:
		return user.first_name
	if user.last_name:
		return user.last_name
	return user.username


def get_object_or_none (model, *args, **kwargs):
	try:
		return model.objects.get(*args, **kwargs)
	except model.DoesNotExist:
		return None


def update_all_users (update):
	usercount = 0
	updatecount = 0
	for user in User.objects.all():
		usercount += 1
		try:
			user.userprofile
		except UserProfile.DoesNotExist:
			updatecount += 1
			if update:
				UserProfile.objects.create(user=user)
	return (usercount, updatecount)


def download_file (url, file_path):
	try:
		request = requests.get(url)
		with open(file_path,'wb') as file:
			file.write(request.content)
		return True
	except IOError as e:
		logger.warning('I/O error({0}): {1}'.format(e.errno, e.strerror))
		return False


def mkdir (dir_name):
	try:
		makedirs(dir_name)
	except OSError as e:
		if e.errno == 17:
			pass


def cache_add (key, data, timeout):
	cache.add(cache_settings['PREFIX'] + key, data, timeout)


def cache_set (key, data, timeout):
	return cache.set(cache_settings['PREFIX'] + key, data, timeout)


def cache_get (key):
	return cache.get(cache_settings['PREFIX'] + key)


def cache_delete (key):
	cache.delete(cache_settings['PREFIX'] + key)


def get_template_content (template_name):
	template_content = False
	for template_settings in settings.TEMPLATES:
		for path in template_settings['DIRS']:
			try:
				template_content = open(path+'/'+template_name, 'r').read()
			except IOError:
				pass
	return template_content


def render_from_string (string, data):
	template = Template(string)
	context = Context(data)
	return template.render(context)


def get_last_restart (request):
	return settings.LAST_RESTART


def has_enough_time_passed (time_checked, time_delta):
	if (not time_checked) or (time_checked < timezone.now() - time_delta):
		return timezone.now()
	else:
		return False


def montage_qrcode (index, url, path):
	call(['qrencode', '-o', path+'/qrcode_'+str(index)+'.png', '-s', '20', url], stdout=NULL)
	call([
		'montage',
		'-label', url,
		path+'/qrcode_'+str(index)+'.png',
		'-font', '/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-C.ttf',
		'-pointsize', '18',
		'-background', 'White',
		'-geometry', '980x1005',
		path+'/qrcode_'+str(index)+'.png'
	])


def montage_qrlist (index, page_size, path):
	montage_call_list = []
	montage_call_list.append('montage')
	for i in xrange(1, page_size+1): montage_call_list.append(path+'/qrcode_'+str(i)+'.png')
	montage_call_list.append('-tile')
	if page_size == 6: montage_call_list.append('2x3')
	if page_size == 24: montage_call_list.append('4x6')
	montage_call_list.append('-geometry')
	montage_call_list.append('+50+50')
	montage_call_list.append(path+'/qr_page_'+str(index)+'.png')
	call(montage_call_list, stdout=NULL)


def async_email (**kwargs):
	kwargs['from_email'] = kwargs.get('from_email') or settings.DEFAULT_FROM_EMAIL
	Thread(target=send_mail, kwargs=kwargs).start()
	return None
