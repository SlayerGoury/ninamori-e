from django.conf import settings
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.http import last_modified
from datetime import datetime, timedelta
#from cms.models import Page


def index (request):
	# TODO deprecate after cms.index
	return render(request, 'index.html', {'message': _('Stub')})


def not_found (request, message=None):
	return render(request, '404.html', {'message': message or _('Page does not exist')}, status=404)


def denied (request, message=None):
	return render(request, '403.html', {'message': message or _('Access denied')}, status=403)


def robots (request):
	return render(request, 'robots.txt', content_type='text/plain')


def sitemap (request):
	urlset = []
	# TODO populate urlset with CMS pages
	return render(request, 'sitemap.xml', {'urlset': urlset}, content_type='text/xml')


def adminhelp (request):
	if request.user.is_staff:
		return render(request, 'admin/help.html')
	else:
		return render(request, '403.html', {'message': _('Staff only')}, status=403)
