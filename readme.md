# Ninamori CMS #

[Deployment and usage briefing](https://ninamori.org/cms/deployment)

[Screenshots](https://ninamori.org/cms/screenshots)

[Feedback form](https://ninamori.org/feedback)

[Base templates](https://bitbucket.org/mlug/ninamori_templates-base)

# Code styling #

* Do not use spaces for indenting, use spaces for aligning
* Do not use space to separate brackets from calls, use space to separate brackets from declarations
* Use single line for beautification and between imports and settings of the variables, use two to separate them from the main declarations
* Always use `from django.utils.translation import ugettext as _` and `{% load i18n %}`, do not leave untranslatable text unless it should be untranslatable
* Use `import unicode_literals` where it is relevant
* Line length is 180, 80 is for weaklings: it´s fine to write your code in shorter lines but do not needlessly rewrite other people´s code
* It is fine to use even longer lines in exceptional cases
* Write your code in a way that is readable to other people
* Ask Goury Gabriev for help if not sure how to style your code or if you have a proposal
