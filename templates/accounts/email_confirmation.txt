{% spaceless %}
{% load i18n %}
{% blocktrans with protocol=protocol site_domain=site_domain app_confirm=app_confirm code=code username=username %}I want to confirm your access to this email account

Please click/open/follow this link for confirmation:
{{ protocol }}://{{ site_domain }}{{ app_confirm }}?code={{ code }}
This code has no expiration date and should be valid for verification unless another code is requested by you.

Your username is: {{ username }}
Your password is secret{% endblocktrans %}
{% endspaceless %}
